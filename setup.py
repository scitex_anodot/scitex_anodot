#!/usr/bin/python
from setuptools import setup, find_packages
import os
import sys
import re

setup(name='scitex_anodot',
    version='0.1',
    description='Anodot Collector for Scitex',
    author='Anodot',
    author_email='moran.cohen+scitex@anodot.com',
    url='https://www.anodot.com',
    python_requires='>= 3.6',
    packages = find_packages(),
    scripts=['bin/nld2anodot.py'],
    install_requires = [
        'requests',
        'pandas'
    ]
)

