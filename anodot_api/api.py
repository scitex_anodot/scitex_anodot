#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import islice
import logging
import requests
from urllib.parse import urljoin
import json
from time import sleep

LOG = logging.getLogger(__name__)

ANODOT_ENV_MAP = {
        'production': 'https://api.anodot.com',
        'poc': 'https://api-poc.anodot.com'
}

DEFAULT_API_CHUNKSIZE = 1000
DEFAULT_API_DELAY = 1

class AnodotPartialErrors(Exception):
    pass

class AnodotAPISession(object):
    """AnodotAPISession"""
    def __init__(self, *,
            token = None,
            anodot_env = 'production',
            api_delay = DEFAULT_API_DELAY, 
            api_chunksize = DEFAULT_API_CHUNKSIZE ):
        """__init__

        :param token:
        :param anodot_env: either `poc` or `production` or a full url of the Anodot API root
        :param api_delay: delay in seconds between consecutive requests
        :param api_chunksize: maximum number of entries (metrics or events) to send in a single request
        """
        
        self.session = requests.Session()
        self.token = token
        self.api_delay = api_delay
        self.api_chunksize = api_chunksize
        self.anodot_root = ANODOT_ENV_MAP.get(anodot_env,anodot_env)

        self._verified_event_sources = {}
        self._verified_event_categories = {}

    @property
    def metrics_endpoint(self):
        return urljoin(self.anodot_root, "api/v1/metrics")

    @property
    def events_endpoint(self):
        return urljoin(self.anodot_root, "/api/v1/user-events")

    @property
    def events_categories_endpoint(self):
        return urljoin(self.anodot_root, "/api/v1/user-events/categories")

    @property
    def events_sources_endpoint(self):
        return urljoin(self.anodot_root, "/api/v1/user-events/sources")

    def post_metrics_chunk(self, content):
        headers = { 'Content-Type':'application/json' }
        LOG.debug("Sending {} metrics in chunk: {} ...".format(len(content), json.dumps(content[:3], indent = 2)))
        
        resp = self.session.post(
                self.metrics_endpoint,
                params = dict(
                    token = self.token,
                    protocol = "anodot20"),
                headers = headers,
                json = content )

        resp.raise_for_status()

        response_data = resp.json()
        if response_data.get('errors'):
            raise AnodotPartialErrors(response_data['errors'])

    def post_metrics(self, metrics, clean = True):
        chunk_size = self.api_chunksize
        for i in range((len(metrics) + chunk_size - 1) // chunk_size):
            LOG.debug("sleeping {} seconds before next chunk".format(self.api_delay))
            if i > 0:
                sleep(self.api_delay)
            chunk = list(islice(metrics,i * chunk_size,(i+1)*chunk_size) )
            LOG.debug("sending chunk of {} metrics".format(len(chunk)))
            self.post_metrics_chunk( chunk )

    def post_events(self, events, auto_add = True):
        for e in events: 
            self._verify_event_source(e['source'], auto_add = auto_add )
        
