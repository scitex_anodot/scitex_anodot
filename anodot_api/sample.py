from datetime import datetime
from collections import OrderedDict

import re
import logging
LOG = logging.getLogger(__name__)

PAT_REPL_UNDERSCORE = re.compile('(/|:| |\.|\+|-|!|\^|&|\[|\]|\{|\}|<|>|~|\||"|\?|=|[^\x00-\x7F])')
PAT_REMOVE = re.compile(r"[\'\(\)]")
PAT_COMPACT = re.compile('_+')
    
def clean_metric_string(s):
    """clean_metric_string
    Cleans `s` by removing characters that may interfere with Anodot search.

    :param s: str - the string to clean.
    """
    try:
        if s == '':
            return 'EMPTY'
        ret = s
        if type(s) == bytes:
            ret = s.decode('ascii', errors = 'ignore')
        elif type(s) != str:
            ret = str(s)
        
        ret = PAT_REMOVE.sub('', ret)
        ret = PAT_REPL_UNDERSCORE.sub('_', ret)
        ret = PAT_COMPACT.sub('_', ret)
        ret = ret.strip('_')
        if ret == '': 
            return 'INVALID'
        return ret
    except Exception as e:
        LOG.error("problem cleaning `{}`".format(s), exc_info = 1)
        raise e

def get_utc_timestamp(dt):
    """returns a UTC timestamp. assuming no timezone = UTC"""
    if dt.tzinfo:
        dt = dt.replace(tzinfo = None) - dt.utcoffset()
    return int(( dt - datetime(1970, 1, 1)).total_seconds())

def props_cmp_key(prop):
    """ convert the prop = (key, val) to lexicographic ordering 
        with what, target_type last
    """
    return ({ 'collector': -1, 'what': 1, 'target_type': 2 }.get(prop[0],0),prop)

def clean_props(props):
    return [(   clean_metric_string( prop_name )[:50],
                clean_metric_string( prop_val )[:150])
                        for (prop_name, prop_val) in sorted(props, key = props_cmp_key) ]

def make_sample( *, dttm = None, props = None, tags = None, value = None , should_clean = True):
    assert dttm is not None
    assert any( (( p[0] == 'what' for p in props )) ), "metric props must contain a `what` prop"
    ret = OrderedDict()
    ret['properties'] = OrderedDict( props if not should_clean else clean_props(props) )
    ret['timestamp'] = get_utc_timestamp(dttm)
    ret['value'] = value
    if tags is not None:
        ret['tags'] = tags #FIXME: should_clean !

    return ret   

def shift_timestamp(sample, timestamp_shift):
    return OrderedDict(sample, timestamp = sample['timestamp'] + timestamp_shift)
