#!/usr/bin/env python3.6
import csv
import itertools
import argparse
import pandas as pd
import os
import sys
import math
from datetime import datetime
import logging
from anodot_api.sample import make_sample
from anodot_api.api import AnodotAPISession

LOG = logging.getLogger('nld2anodot')

def jmp_file_dttm(basename):
    try:
        return datetime.strptime(basename, 'Jmp_%Y.%m.%d_%H.%M.%S.csv')
    except Exception as e:
        LOG.debug(f"{basename} not jmp format")

def get_df(jmpfile):
    df = pd.read_csv(jmpfile)
    df.rename( columns = {'PmW Norm To Single Sub Frame AVG[Um]':'pmw_norm'}, inplace = True )
    df['count'] = 1
    del df['Session Time']
    del df['Unnamed: 10']
    return df

def get_warning_counts(df):
    nozzle_warning_per_die_df = df.groupby(['Color', 'Bar','Pen','Die', 'Nozzle Warning'], as_index = False).count()
    for i in nozzle_warning_per_die_df.index:
        row = nozzle_warning_per_die_df.loc[i]
        yield ( [ ( "what", 'nozzle_warning_count' ), ("target_type","counter") ] + 
                    [ ( key, row[key] ) for key in [ 'Color', 'Bar','Pen','Die', 'Nozzle Warning' ] ], row['count'] )


DIE_LEVEL_GROUP = [ 'Color', 'Bar','Pen','Die' ]
def die_level_group_props(i):
    return list(zip(DIE_LEVEL_GROUP,list(i)))

def get_die_level_aggs(die_group_df):
    # Aggregation DFs
    
    dg_mean = die_group_df.mean()
    dg_max = die_group_df.max()
    dg_min = die_group_df.min()
    dg_std = die_group_df.std()

    for agg_name, metric_df in ('mean', dg_mean), ('max',dg_max), ('min',dg_min), ('stddev', dg_std):
        for i in metric_df.index:
            row = metric_df.loc[i]
            yield ( [ ( "what", f"pmw_norm_{agg_name}" ), ("target_type", "gauge") ] + die_level_group_props(i), row['pmw_norm'] )
            yield ( [ ( "what", f"line_merit_{agg_name}" ), ("target_type", "gauge" ) ] + die_level_group_props(i), row['Line Merit'] )

def get_die_level_quantile_metrics(die_group_df):
    # Quantile DF
    dg_quantiles = die_group_df.quantile([0.20,0.5,0.80])

    for i in dg_quantiles.index:
        percentile = int(i[4] * 100)
        row = dg_quantiles.loc[i]
        yield ( [ ("what", f"pmw_norm_p{percentile}"), ("target_type", "gauge" ) ] + die_level_group_props(i), row['pmw_norm'] )
        yield ( [ ("what", f"line_merit_p{percentile}"), ("target_type", "gauge" ) ] + die_level_group_props(i), row['Line Merit'] )

def produce_metrics(jmpfile, dttm):
    # Base DF
    df = get_df(jmpfile)
    # Clean DF from special values
    df_clean = df[(df["Line Merit"] < 999) & (df["pmw_norm"] < 999) & (df['Nozzle Warning'] != 'OUT')]
    die_group_df = df_clean.groupby(DIE_LEVEL_GROUP, as_index = True)

    return itertools.chain(
            get_warning_counts(df),
            get_die_level_aggs(die_group_df),
            get_die_level_quantile_metrics(die_group_df)
            )
#            get_warning_counts(df), 
#            get_quantile_metrics(die_group_df)
#            ):
#        yield props, value

def collect_from_file(jmpfile, dttm, callback):
    metrics = list(produce_metrics(jmpfile, dttm))
    callback([ (dttm,) + m for m in metrics ])

def handle_directory(path, callback):
    assert os.path.isdir(path)
    LOG.info(f"handling directory {path}")
    complete_subdir_path = os.path.join(path,'completed')
    error_subdir_path = os.path.join(path,'error')
    if not os.path.isdir(complete_subdir_path): os.mkdir(complete_subdir_path)
    if not os.path.isdir(error_subdir_path): os.mkdir(error_subdir_path)

    files = [ (jmp_file_dttm(fn), fn) for fn in os.listdir(path) if fn.endswith('.csv') ]
    files = sorted(list( (( ( dttm, fn ) for ( dttm, fn ) in files if dttm is not None )) ))
    
    for dttm, fn in files:
        try:
            jmpfile = os.path.join(path, fn)
            collect_from_file( jmpfile, dttm, callback)
            os.rename(jmpfile, os.path.join(complete_subdir_path, fn) )
        except Exception as e:
            LOG.warn(f"Exception occurred and {fn} ignored", exc_info = True)
        #    os.rename(jmpfile, os.path.join(error_subdir_path, fn) )

def metrics_sender(anodot_env, token, constant_props):
    extra_props = [ tuple(s.split('=')[:2]) for s in constant_props ]
    session = AnodotAPISession( anodot_env = anodot_env, token = token )
    def send_metrics(metrics):
        print(f"sending {len(metrics)} metrics: {metrics[:3]}...")
        samples = [
            make_sample(dttm = dttm, props = props + extra_props, value = float(value))
                for dttm, props, value in metrics 
                if not ( math.isnan(value) or math.isinf(value) ) ]
        session.post_metrics(samples)

    return send_metrics

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('directories', nargs = "*")
    parser.add_argument('-t', '--token', required = True)
    parser.add_argument('-e', '--anodot-env', default = 'poc')
    parser.add_argument('-c', '--constant-property', action = 'append', help = "add key=value as a definition for the query", default = [])

    args = parser.parse_args()

    for directory in args.directories:
        handle_directory(directory, metrics_sender(args.anodot_env, args.token, args.constant_property))

